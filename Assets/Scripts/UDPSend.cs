﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System.Text;
using System;
using UnityEngine.UI;
using System.Net;

public class UDPSend : MonoBehaviour {
    public Button button;
    public int port;
    public string message;

    private IPEndPoint ip;

    public void Awake() {
        this.button.onClick.AddListener(() => { sendMessage(); });
        ip = new IPEndPoint(IPAddress.Broadcast, port);
    }

    public void sendMessage() {
        var client = new UdpClient();
        client.Connect(ip);
        client.EnableBroadcast = true;

        Byte[] sendBytes = Encoding.ASCII.GetBytes(message);
        try {
            client.Send(sendBytes, sendBytes.Length);
        } catch(Exception e) {
            Debug.Log(e.Message);
        }
    }
}
